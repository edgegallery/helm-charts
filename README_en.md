# Edgegallery Helm Chart
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Deploy your own private Edgegallery.

## Prerequisites
* Kubernetes 1.6+
* Helm 3+
* [If enabled] nfs server and RW access to it
* [If enabled] nfs-client-provisioner for dynamic provisioning
```
helm install nfs-client-provisioner --set nfs.server=<nfs_sever_ip> --set nfs.path=<nfs_server_directory> stable/nfs-client-provisioner 
```
* [If enabled] nginx-ingress-controller for ingress
```
kubectl label node <node_name> node=edge
helm install nginx-ingress-controller stable/nginx-ingress --set controller.kind=DaemonSet --set controller.nodeSelector.node=edge --set controller.hostNetwork=true
```

## Edgegallery Installation（Online）

1. clone project

```shell
$ git clone https://gitee.com/edgegallery/helm-charts.git
```

2. install project on demand

```shell
## example
$ cd service-center
$ helm install my-service-center . 
```


## Edgegallery Installation（Offline）
* Install Guide: [link](https://gitee.com/edgegallery/installer/blob/Release-v1.5/ansible_install/README-cn.md)
